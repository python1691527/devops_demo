FROM python:3.12-alpine3.18

LABEL authors="flavi"
COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

COPY . sites
WORKDIR /sites

EXPOSE 8000

ENTRYPOINT ["python", "manage.py"]
CMD ["runserver", "0.0.0.0:8000"]